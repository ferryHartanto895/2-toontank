// Fill out your copyright notice in the Description page of Project Settings.


#include "PawnTurret.h"
#include "Kismet/GamePlayStatics.h"
#include "PawnTank.h"

// Called when the game starts or when spawned
APawnTurret::APawnTurret() 
{

}
void APawnTurret::BeginPlay()
{
	Super::BeginPlay();
	GetWorld()->GetTimerManager().SetTimer(fireRateTimerHandle, this, &APawnTurret::CheckFireCondition, fireRate, true);
	playerPawn = Cast<APawnTank>(UGameplayStatics::GetPlayerPawn(this, 0));
	
}

void APawnTurret::HandleDestruction()
{
	//if destroyed
	Super::HandleDestruction();
	Destroy();
}



// Called every frame 
void APawnTurret::Tick(float DeltaTime)
{
	//get player location
	Super::Tick(DeltaTime);
	if (!playerPawn || ReturnDistanceToPlayer() > fireRange)
	{
		return;
	}

	RotateTurret(playerPawn->GetActorLocation());
}

void APawnTurret::CheckFireCondition()
{
	if (!playerPawn || !playerPawn->getPlayerAlive())
	{
		return;
	}
	//if player is at range
	if(ReturnDistanceToPlayer() <= fireRange)
	{
		Fire();
	}
}

//calculate the distance to player
float APawnTurret::ReturnDistanceToPlayer()
{
	if (!playerPawn)
	{
		return 0.0f;
	}

	return FVector::Dist(playerPawn->GetActorLocation(), GetActorLocation());
	
}
