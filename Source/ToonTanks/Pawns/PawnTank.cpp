// Fill out your copyright notice in the Description page of Project Settings.


#include "PawnTank.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"


APawnTank::APawnTank()
{
	//create spring arm for camera
	springArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("Spring Arm"));
	springArm->SetupAttachment(RootComponent);

	camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	camera->SetupAttachment(springArm);
}
void APawnTank::CalculateInputMove(float Value)
{
	//calculate the speed
	moveDir = FVector(Value * moveSpeed * GetWorld()->DeltaTimeSeconds, 0, 0);
}
void APawnTank::calculateRotateInput(float Value)
{
	//calculate the rotaion
	float rotateAmount = Value * rotateSpeed * GetWorld()->DeltaTimeSeconds;
	FRotator rotation = FRotator(0,rotateAmount,0);
	rotDir = FQuat(rotation);
}
void APawnTank::Move()
{
	AddActorLocalOffset(moveDir, true);
}
void APawnTank::Rotate()
{
	AddActorLocalRotation(rotDir, true);
}
// Called when the game starts or when spawned
void APawnTank::BeginPlay()
{
	Super::BeginPlay();
	playerRefController = Cast<APlayerController>(GetController());
}

void APawnTank::HandleDestruction()
{
	Super::HandleDestruction();

	bIsPlayerAlive = false;

	SetActorHiddenInGame(true);
	SetActorTickEnabled(false);

}

bool APawnTank::getPlayerAlive()
{
	//check if player alive
	return bIsPlayerAlive;
}

// Called every frame
void APawnTank::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Rotate();
	Move();

	if (playerRefController)
	{
		FHitResult traceHitResult;
		playerRefController->GetHitResultUnderCursor(ECC_Visibility, false, traceHitResult);
		FVector hitLocation = traceHitResult.ImpactPoint;

		RotateTurret(hitLocation);
	}
}

// Called to bind functionality to input
void APawnTank::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveForward", this, &APawnTank::CalculateInputMove);
	PlayerInputComponent->BindAxis("Turn", this, &APawnTank::calculateRotateInput);
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &APawnTank::Fire);

}